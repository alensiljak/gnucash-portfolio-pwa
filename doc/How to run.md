# This script will run the server and the client.
# Can be used with Termux:Widget.

export TZ=$(getprop persist.sys.timezone)
export SHELL=$(which bash)

# Run the server
# python ~/server/app.py &
gpweb &

# Run the browser
am start --user 0 -a android.intent.action.VIEW -d "http://localhost:5000" &

wait

echo Complete!
