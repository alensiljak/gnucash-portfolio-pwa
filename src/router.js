import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './views/Home'

Vue.use(VueRouter)

const routes = [
    { path: '/', component: Home },
    { path: '/aa', component: () => import('./views/AssetAllocation.vue') },
    { path: '/settings', component: () => import('./views/Settings.vue') },
    { path: '/purchase', component: () => import('./views/PurchaseCalc.vue') },
    { path: '/portfoliovalue', component: () => import('./views/PortfolioValue.vue') }
  ]

  const router = new VueRouter({
    mode: 'history',
    routes: routes,
    hashbang: false
  });
  
  export default router;