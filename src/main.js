import Vue from 'vue'
import App from './App'
import router from './router'

import Buefy from 'buefy'
//import 'buefy/dist/buefy.css'
Vue.use(Buefy)

//import 'bulma/css/bulma.css'

Vue.config.productionTip = false

// register service worker here?
navigator.serviceWorker && navigator.serviceWorker.register('./serviceWorker.js')
  .then(function(registration) {
    console.log('Excellent, registered with scope: ', registration.scope);
});

new Vue({
    router,
    render: h => h(App)
  }).$mount('#app')
