/**
 * Settings.
 * Represents the settings object. It is loaded and saved to the local storage.
 */
import localforage from "localforage";

localforage.config({
    name: "gc-portfolio"
  });
  
var defaultSettings = {
    serverUrl: ''

};

var configManager = {
    load() {
        // load the settings here.
        return localforage.getItem("settings");
    },
    save(settings) {
        localforage.setItem("settings", settings);
    },
    defaultSettings: defaultSettings
}

export default configManager;
