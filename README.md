# gnucash-portfolio-pwa

The Web / PWA UI for GnuCash Portfolio

https://gnucash-portfolio.netlify.com/

[deprecated](https://alensiljak.gitlab.io/gnucash-portfolio-pwa)

[![pipeline status](https://gitlab.com/alensiljak/gnucash-portfolio-pwa/badges/master/pipeline.svg)](https://gitlab.com/alensiljak/gnucash-portfolio-pwa/commits/master)

## Introduction

This is the Progressive Web Application (PWA), a Web user interface (UI) for GnuCash Portfolio suite exposed through Web API. It includes:

- asset allocation
- price database
- gnucash portfolio
  - reports
  - analysis
  - currency conversion
- gnucash import
- gnucash scheduler
- wallet (tracking transactions)

and potentially other functionality. It is a Swiss-knife app that encapsulates the above libraries and provides a graphical user interface (GUI) to them. 

It is a multi-platform app which can run on desktop and mobile operating systems.

Requires the Gnucash Portfolio WebAPI server.

## Technologies

Using Parcel to package the application.
Vue.js for the application.
Bulma for CSS styling.

## Development

Icons are used from Iconify.design. 

The compiled files go into /public directory, to be published automatically.

`npm run dev` starts a dev web server.
`npm run build` builds the site.

## Deployment

Deployed automatically by Netlify.
